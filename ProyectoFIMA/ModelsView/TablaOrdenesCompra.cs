﻿using ProyectoFIMA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProyectoFIMA.ModelsView
{
    public class TablaOrdenesCompra
    {
        public int id_orden { set; get; }
        public string descripcion { set; get; }
        public string tarea { set; get; }

        public List<Tareas> tareas { set; get; }
        public bool urgente { set; get; }
        public string estatus { set; get; }
    }
}
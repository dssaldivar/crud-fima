﻿using ProyectoFIMA.Models;
using ProyectoFIMA.ModelsView;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProyectoFIMA.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<TablaOrdenesCompra> lst = new List<TablaOrdenesCompra>();
            using (var contex = new fimaEntities())
            {
                var ordenescompra = contex.OrdenesCompra.ToList();
                foreach (var o in ordenescompra) 
                {
                    var tareas = new string[o.Tareas.Count];

                    int index = 0;                    
                    foreach (var tarea in o.Tareas)
                    {
                        tareas[index] = tarea.descripcion;
                        index++;
                    }
                    lst.Add(new TablaOrdenesCompra
                    {
                        id_orden = o.ot_id,
                        descripcion = o.descripcion,
                        urgente = o.urgente,
                        estatus = o.estatus,
                        tarea = string.Join(",",tareas)
                    });
                };
            }
            return View(lst);
        }

        public ActionResult Registro()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        [HttpPost]
        public ActionResult Registrar(TablaOrdenesCompra o)
        {
            try
            {
                using (var context = new fimaEntities())
                {
                    OrdenesCompra nuevaOrden = new OrdenesCompra
                    {
                        descripcion = o.descripcion,
                        urgente = o.urgente,
                        estatus = "A"
                    };
                    context.OrdenesCompra.Add(nuevaOrden);
                    context.SaveChanges();

                    if (o.tareas != null)
                    {
                        foreach (var t in o.tareas)
                        {
                            context.Tareas.Add(new Tareas
                            {
                                descripcion = t.descripcion,
                                ot_id = nuevaOrden.ot_id,
                                OrdenesCompra = nuevaOrden
                            });
                        }
                        context.SaveChanges();
                    }
                    return RedirectToAction("index");
                }

            }
            catch (DbEntityValidationException e)
            {
                throw;
            }
            return RedirectToAction("Registro");
        }

        [HttpPost]
        public ActionResult Update(int id)
        {
            var newEstatus = "";
            using (var context = new fimaEntities())
            {
                var update = context.OrdenesCompra.SingleOrDefault(x => x.ot_id == id);
                update.estatus = update.estatus.Equals("A")?"C":"A";
                newEstatus = update.estatus;
                context.SaveChanges();
            }
            return Json(new { estatus = newEstatus });
        }
        public ActionResult Contact()
        {
            ViewData["Telefono"] = "+52 1 81 8800 3288";
            return View();
        }
    }
}
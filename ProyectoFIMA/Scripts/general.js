function actualizarEstatus(id){
     jQuery.ajax({
                url: '/Home/Update',
                method: 'post',
                data: {
                    id: id
                }
            }).done(function (result) {
                console.log(result)
            });
}

function agregarTareas() {
    var contenedor = $('.tareas');
    contenedor.show();
    var input = `<div class="form-group">
            <label for="Tareas" class="col-sm-4 control-label">Descripcion Tarea </label>
            <div class="col-sm-8">
                <input type="text"  />
                <input type="button" onclick="eliminarTarea(this)" value="eliminar"></input>
            </div>
        </div>`;
    contenedor.append(input);
    update();
}
function eliminarTarea(element) {
    element.parentElement.parentElement.remove();
    update();
    var contenedor = $('.tareas');
    if (contenedor.children().length == 0) {
        contenedor.hide();
    }
}
function update() {
    $('.tareas').find('input[type="text"]').each(function (index, element) {
        element.setAttribute('name', "tareas[" + index + "].descripcion");
    })
}
function registar() {
    console.log("Validar");
    var correcto = true;
    $('input[type="text"]').each(function (indice, elemento) {
        if (elemento.value.length >= 20 && elemento.value.length <= 255) {
            
        } else {
            correcto = false;
            return false;
        }
    });

    if (correcto) {
        //Guardar
        $('#form-registro').submit();
    }
    else {
        alert('Los input no cumplen la longitud minima de 20 caracteres');
    }
}